## v1.1.0
*(2021-01-15)*

#### Features
* Přidání příkazu pro získání tagu další verze

---
## v1.0.3
*(2020-12-25)*

#### Bugfixes
* Add dependancy injenction for symfony 4.4

---
## v1.0.2
*(2018-11-23)*

#### Bugfixes
* Úprava configů pro IDE
* [prava p59kayu pro commit v gitu

---
## v1.0.1
*(2018-07-27)*

#### Bugfixes
* Oprava defaultů konfigu
* Při vydání nové verze commitne všechny změny

#### Refactoring
* Odstranění nepoužitého kodu

#### Documentation
* Upravení readme a překlepů
* Doplnění informací do composeru

---
## v1.0.0
*(2018-07-11)*

#### Features
* [#6](https://gitlab.com/james.fruhbauer/versioning-bundle/issues/6) Přidání templatů Normal a Simple pro generování changelogu
* [#6](https://gitlab.com/james.fruhbauer/versioning-bundle/issues/6) Přidání možnosti nastavit si custom template pro changelog
* Přidání možnosti spuštění vlastních kódů při vydání nové verze

#### Documentation
* Přidání možností nastavení configu do readme
* Přidání obarvení ukázek kodu do readme

---
## v0.3.0
*(2018-03-12)*

#### Features
* [#7](https://gitlab.com/james.fruhbauer/versioning-bundle/issues/7) Přidána možnost negenerovat skupinu do changelogu

---

## v0.2.1
*(2018-03-11)*

#### Refactoring
* Změna namespace a názvu balíku podle konvencí

---

## v0.2.0
*(2018-03-11)*

#### Features
* [#5](https://gitlab.com/james.fruhbauer/versioning-bundle/issues/5) Přidání nastavení configu per projekt přes yaml

#### Refactoring
* Upravení názvů příkazů za produkční
* Upravení názvu modelu pro získání providera

#### Documentation
* Upravení readme a contributing

---

## v0.1.0
*(2018-03-09)*

#### Features
* [#2](https://gitlab.com/james.fruhbauer/versioning-bundle/issues/2) Základní struktura symfony bundlu
* [#2](https://gitlab.com/james.fruhbauer/versioning-bundle/issues/2) Přidání interfacu pro providery
* [#3](https://gitlab.com/james.fruhbauer/versioning-bundle/issues/3) Přidání nastavení pro verzování sebe sama
* [#4](https://gitlab.com/james.fruhbauer/versioning-bundle/issues/4) Přidání základního verzování a generování changelogu

#### Bugfixes
* Resetování minor a patchů při zvednutí major verze

---

## v0.0.1
*(2018-03-09)*

#### Features
* [#1](https://gitlab.com/james.fruhbauer/versioning-bundle/issues/1) Přidání composer.json

---
