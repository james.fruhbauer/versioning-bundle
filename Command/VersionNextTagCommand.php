<?php

namespace Levit\VersioningBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Levit\VersioningBundle\Model\ChangelogModel;

class VersionNextTagCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('version:next:tag')
            ->setDescription('Zobrazí další tag');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $changelog = new ChangelogModel();
        $text = $changelog->getVersionNextTag();

        $output->writeln($text);
    }

}
