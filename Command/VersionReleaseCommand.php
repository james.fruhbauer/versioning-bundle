<?php

namespace Levit\VersioningBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Levit\VersioningBundle\Model\ChangelogModel;
use Levit\VersioningBundle\Model\ConfigModel;

class VersionReleaseCommand extends Command
{
    protected function configure()
    {
        $config = new ConfigModel();

        $this
            ->setName('version:release')
            ->setDescription('Vydá novou verzi, otagovanou s vygenerovaných changelogem a comitnutým');

        foreach($config->getCustomParamList() as $param)
        {
            $mode = ($param['mode'] == 'REQUIRED' ? 1 : 2);
            $default = (isset($param['default']) ? $param['default'] : null);

            $this->addArgument($param['name'], $mode, $param['description'], $default);
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $isConfirmed = $io->confirm('Release new version?', true);

        if ($isConfirmed) {
            $config = new ConfigModel();
            $customParamList = array();

            foreach($config->getCustomParamList() as $param)
            {
                $customParamList[$param['name']] = $param;
                $customParamList[$param['name']]['value'] = $input->getArgument($param['name']);
            }

            $changelog = new ChangelogModel();
            $text = $changelog->releaseVersion($customParamList);

            $io->success('Release ' . $text . ' has been completed.');
        }
    }

}
