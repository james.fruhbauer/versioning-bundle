<?php

namespace Levit\VersioningBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Levit\VersioningBundle\Model\ChangelogModel;

class VersionUnpublishedCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('version:unpublished')
            ->setDescription('Zobrazí připravovanou verzi se změnamy které obsahuje');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $changelog = new ChangelogModel();
        $text = $changelog->getVersionChangelog();

        $output->writeln($text);
    }

}
