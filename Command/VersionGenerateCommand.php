<?php

namespace Levit\VersioningBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Levit\VersioningBundle\Model\ChangelogModel;

class VersionGenerateCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('version:generate')
            ->setDescription('Vygeneruje changelog pro novou verzi, bez commitutí a tagu');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $changelog = new ChangelogModel();
        $text = $changelog->generateVersionChangelog();

        $io->success('The changelog for ' . $text . ' has been generated.');
    }

}
