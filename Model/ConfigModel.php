<?php

namespace Levit\VersioningBundle\Model;

use Symfony\Component\Yaml\Yaml;

class ConfigModel
{

    /**
     * @var array
     */
    private $config;

    public function __construct()
    {
        $this->config = Yaml::parseFile('versioning.yml');
    }

    /**
     * @return string
     */
    public function getProvider()
    {
        return $this->config['provider'];
    }

    /**
     * @return array
     */
    public function getGroupList()
    {
        $groupList = array();

        foreach ($this->config['groupList'] as $group) {
            $groupList[$group['groupName']] = array();
        }

        return $groupList;
    }

    /**
     * @return string
     */
    public function getRegexKeyword()
    {
        $keywordList = array();

        foreach ($this->config['groupList'] as $group) {
            $keywordList[] = implode('|', $group['keywordList']);
        }

        return implode('|', $keywordList);
    }

    public function getGroupByKeyword($keyword)
    {
        $group = "";

        foreach ($this->config['groupList'] as $groupItem) {
            if (in_array(strtoupper($keyword), $groupItem['keywordList'])) {
                $group = $groupItem;
            }
        }

        return $group;
    }

    public function getTicketPatern()
    {
        return (isset($this->config['ticketPatern']) ? $this->config['ticketPatern'] : '');
    }

    public function getTemplate()
    {
        return (isset($this->config['template']) ? $this->config['template'] : 'Normal');
    }

    public function getTemplatePath()
    {
        return $this->config['templatePath'];
    }

    public function getCustomCommandList()
    {
        return (isset($this->config['customCommandList']) ? $this->config['customCommandList'] : array());
    }

    public function getCustomParamList()
    {
        return (isset($this->config['customParamList']) ? $this->config['customParamList'] : array());
    }

}
