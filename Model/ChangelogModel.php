<?php

namespace Levit\VersioningBundle\Model;

use Levit\VersioningBundle\Model\ProviderModel;
use Levit\VersioningBundle\Provider\ProviderInterface;
use Levit\VersioningBundle\Model\ConfigModel;
use Levit\VersioningBundle\Model\LogModel;

class ChangelogModel
{

    /**
     * @var ConfigModel
     */
    private $config;

    /**
     * @var ProviderInterface
     */
    private $provider;

    public function __construct()
    {
        $this->config = new ConfigModel();

        $providerFactory = new ProviderModel();
        $this->provider = $providerFactory->getProvider($this->config);
    }

    public function getVersionChangelog()
    {
        $logModel = new LogModel($this->config, $this->provider);
        $changelogText = $logModel->getChangelogText();

        return $changelogText;
    }

    public function getVersionNextTag()
    {
        $logModel = new LogModel($this->config, $this->provider);

        return $logModel->getNewVersion();
    }

    public function generateVersionChangelog()
    {
        $logModel = new LogModel($this->config, $this->provider);
        $changelogText = $logModel->getChangelogText();

        $filename = "CHANGELOG.md";
        $changelogTextOriginal = file_get_contents($filename);

        file_put_contents($filename, $changelogText . $changelogTextOriginal);

        return $logModel->getNewVersion();
    }

    public function releaseVersion($customParamList)
    {
        $version = $this->generateVersionChangelog();

        $customParamList['version'] = array(
            'name' => 'version',
            'placeholder' => '#VERSION#',
            'value' => $version,
        );

        $this->customCommand($customParamList);

        $this->provider->commitChangelog("Changelog " . $version);
        $this->provider->createTag($version);

        return $version;
    }

    private function customCommand($customParamList)
    {
        foreach($this->config->getCustomCommandList() as $command)
        {
            $command = $command['command'];

            foreach($customParamList as $customParam)
            {
                $command = str_replace($customParam['placeholder'], $customParam['value'], $command);
            }

            shell_exec( $command );
        }
    }

}
