<?php

namespace Levit\VersioningBundle\Model;

use Levit\VersioningBundle\Model\ConfigModel;
use Levit\VersioningBundle\Provider\ProviderInterface;

class LogModel
{

    /**
     * @var ConfigModel
     */
    private $config;

    /**
     * @var ProviderInterface
     */
    private $provider;

    /**
     * @var array
     */
    private $changelist = array();

    /**
     * @var string
     */
    private $lastVersion;

    /**
     * @var string
     */
    private $newVersion;

    /**
     * @var array
     */
    private $upgrade = array(
        'MAJOR' => false,
        'MINOR' => false,
        'PATCH' => false,
    );

    public function __construct(ConfigModel $config, $provider)
    {
        $this->config = $config;
        $this->provider = $provider;
        $this->lastVersion = $this->provider->getLastVersion();
        $this->setChangelist();
        $this->setNewVersion();
    }

    private function setChangelist()
    {
        $log = $this->provider->getLogFromTag($this->lastVersion);
        $this->changelist = $this->config->getGroupList();
        $matches = array();

        preg_match_all("~(" . $this->config->getRegexKeyword() . "): \s*(.*)~i", $log, $matches);

        foreach ($matches[1] as $key => $keyword) {
            $group = $this->config->getGroupByKeyword($keyword);

            if (!empty($group) && (!isset($group['changelog']) || $group['changelog'] == true)) {
                $this->changelist[$group['groupName']][] = $matches[2][$key];
                $this->upgrade[$group['upgrade']] = true;
            } elseif (!empty($group)) {
                $this->upgrade[$group['upgrade']] = true;
            }
        }
    }

    private function setNewVersion()
    {
        list($major, $minor, $patch) = array_map('intval', explode(".", substr($this->lastVersion, 1)));

        if ($this->upgrade['MAJOR']) {
            $major++;
            $minor = 0;
            $patch = 0;
        } else if ($this->upgrade['MINOR']) {
            $minor++;
            $patch = 0;
        } else if ($this->upgrade['PATCH']) {
            $patch++;
        }

        $this->newVersion = "v{$major}.{$minor}.{$patch}";
    }

    public function getNewVersion()
    {
        return $this->newVersion;
    }

    private function getTemplatePath()
    {
        if($this->config->getTemplate() == "Custom")
        {
            $templatePath = $this->config->getTemplatePath();
        }
        else
        {
            $templatePath = __DIR__ . '/../Resources/views/' . $this->config->getTemplate();
        }

        return $templatePath;
    }

    public function getChangelogText()
    {
        $template = file_get_contents($this->getTemplatePath() . '/index.md');
        $templateVersion = str_replace('{{VERSION}}', $this->newVersion, $template);
        $templateDate = str_replace('{{DATE}}', \date("Y-m-d"), $templateVersion);
        $templateContent = str_replace('{{GROUPLIST}}', $this->getFormatedText(), $templateDate);

        return $templateContent;
    }

    private function getFormatedText()
    {
        $textGroup = "";
        foreach ($this->changelist as $groupName => $messageList) {
            if (!empty($messageList)) {
                $textMessage = "";
                $reversed_array = array_reverse($messageList);
                foreach ($reversed_array as $value) {
                    $templateMessage = file_get_contents($this->getTemplatePath() . '/message.md');
                    $templateMessageContent = str_replace('{{MESSAGE}}', $this->getTicketLink($value), $templateMessage);
                    $textMessage .= $templateMessageContent;
                }

                $templateGroup = file_get_contents($this->getTemplatePath() . '/group.md');
                $templateGroupTitle = str_replace('{{GROUPTITLE}}', $groupName, $templateGroup);
                $templateGroupContent = str_replace('{{MESSAGELIST}}', $textMessage, $templateGroupTitle);
                $textGroup .= $templateGroupContent;
            }
        }

        return $textGroup;
    }

    private function getTicketLink($message)
    {
        preg_match_all("~#([0-9]+)~", $message, $matches);

        foreach ($matches[0] as $key => $value) {
            $link = '[' . $value . '](' . $this->config->getTicketPatern() . $matches[1][$key] . ')';
            $message = str_replace($value . ' ', $link . ' ', $message);
        }

        return $message;
    }

}
