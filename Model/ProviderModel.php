<?php

namespace Levit\VersioningBundle\Model;

use Levit\VersioningBundle\Provider\ProviderInterface;
use Levit\VersioningBundle\Model\ConfigModel;

class ProviderModel
{

    /**
     * @param ConfigModel $config
     * @return ProviderInterface
     */
    public function getProvider(ConfigModel $config)
    {
        $className = "\\Levit\\VersioningBundle\\Provider\\" . $config->getProvider() . "Provider";

        if (!class_exists($className)) {
            throw new \Exception("Daný provider neexistuje, před použitím prosím doplňte");
        }

        $class = new $className();

        return new $class();
    }

}
