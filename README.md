# Versioning Bundle

Symfony bundle for semantick versioning and generating changelog from commit messages.

## Usages

### Installation

Update your `composer.json`

```json
"require-dev": {
    ...
    "levit/versioning-bundle": "1.*"
},
```

Update you `AppKernel.php` and register bundle

```php
if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
    ...
    $bundles[] = new Levit\VersioningBundle\LevitVersioningBundle();
}
```

Create `versioning.yml` in root of project with default settings

```yml
provider: Git
groupList:
    - { groupName: Release, keywordList: [RELEASE], upgrade: MAJOR, changelog: false }
    - { groupName: Features, keywordList: [FEATURE], upgrade: MINOR }
    - { groupName: Bugfixes, keywordList: [HOTFIX, FIX, BUG, BUGFIX], upgrade: PATCH }
    - { groupName: Refactoring, keywordList: [REFACTORING], upgrade: PATCH }
    - { groupName: Documentation, keywordList: [DOCS], upgrade: PATCH }
```

### Commit messages

*Commit message* musí mít specifický nadpis: 
(každý commit s klíčovým slovem se vygeneruje do changelogu, ostatní budou ignorovány)

- první je klíčové slovo označující závažnost (HOTFIX, FIX, BUG, FEATURE, DOCS, REFACTORING)
- za typem je nepovinné číslo tiketu i se sharpem
- stručný popis co přidává nebo opravuje

Příklady:

```
Hotfix: Oprava zobrazení homepage
```

```
Feature: Přidává zobrazení uživatelského jména v top baru
```

```
Docs: #1 Návod na přidávání funkcí
```

Až budete mít na localhostu vše ověřené a funkční, commitnuté a připravené
na produkci, můžete si prohlédnout budoucí changelog v consoly.

```
php bin/console version:unpublished
```

Pokud jste spokojeni mužete spustit automatické vygenerování verze.

```
php bin/console version:release
```

Pro verzování je nutné mít všechny změny commitnuté. Předchozí commit zprávy se
automaticky připíšou do CHANGELOG.md a provede se commit s číslem nové verze
a vytvoří se tag. Pokud se Vám změny v changelogu v tuto chvíli nelíbí,
máte možnost jej upravit a pomocí `amend` je ještě připsat k release commitu.

Poté master i nově vytvořený tag pushnete.

Nová verze se nemusí vytvářet pro každou změnu ale pouze pokud máte záměr nahrát 
změny na produkci.

Pokud máte změny připraveny ale chcete si zobrazit jak při vydání nové verze 
bude vypadat changelog může si jej vygenerovat pomocí následujícího příkazu.

```
php bin/console version:generate
```

Tento příkaz vygeneruje changelog, ale necommitne jej a nevytvoří tag s novou 
verzí. Následně stačí jen revertnout změny na changelogu a můžete pokračovat 
dál ve vývoji.

### Configurations

```yml
provider: Git
ticketPatern: 'https://gitlab.com/james.fruhbauer/versioning-bundle/issues/'
template: Normal #[Normal|Simple|Custom]
# if you use custom template set path
# template: Custom
# templatePath: 'MyCustomTemplateBundle/Resources/views/Custom'
groupList:
    - { groupName: Release, keywordList: [RELEASE], upgrade: MAJOR, changelog: false }
    - { groupName: Features, keywordList: [FEATURE], upgrade: MINOR }
    - { groupName: Bugfixes, keywordList: [HOTFIX, FIX, BUG, BUGFIX], upgrade: PATCH }
    - { groupName: Refactoring, keywordList: [REFACTORING], upgrade: PATCH }
    - { groupName: Documentation, keywordList: [DOCS], upgrade: PATCH }
# custom command for example build CSS from SASS
# placeholder #VERSION# is available with current version
# customCommandList:
#     - { name: 'Npm Build', command: 'npm run build-prod --env.template=#TEMPLATE#' --env.version=#VERSION# }
# customParamList: 
#     - { name: template, placeholder: '#TEMPLATE#', mode: REQUIRED, description: 'Name of template' }
```

## Resources

* [Contributing](CONTRIBUTING.md)
* [Changelogu](CHANGELOG.md)
* [Report issues](https://gitlab.com/james.fruhbauer/versioning-bundle/issues/)
