<?php

namespace Levit\VersioningBundle\Provider;

use Levit\VersioningBundle\Provider\ProviderInterface;

class GitProvider implements ProviderInterface
{

    public function getLastVersion()
    {
        return shell_exec( 'git describe --tags $(git rev-list --tags --max-count=1)' );
    }

    public function getLogFromTag($tag)
    {
        return shell_exec( 'git log ' . \trim( $tag ) . '..HEAD' );
    }

    public function commitChangelog($message)
    {
        shell_exec( 'git add .' );
        shell_exec( 'git commit -m "' . $message . '"' );
    }

    public function createTag($tag)
    {
        shell_exec( 'git tag -a ' . $tag . ' -m "' . $tag . '"' );
    }

}
