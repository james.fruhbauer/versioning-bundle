<?php

namespace Levit\VersioningBundle\Provider;

interface ProviderInterface
{

    /**
     * @return string
     */
    public function getLastVersion();

    /**
     * @param string $tag
     * @return string
     */
    public function getLogFromTag($tag);

    /**
     * @param string $message
     * @todo Zjistit co bude vracet
     */
    public function commitChangelog($message);

    /**
     * @param string $tag
     * @todo Zjistit co bude vracet
     */
    public function createTag($tag);

}
